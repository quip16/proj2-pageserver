from flask import Flask, render_template, abort
import os.path

app = Flask(__name__)
app = Flask(__name__, template_folder="pages/")

forbidden = ["..", ".","~"]

@app.errorhandler(403)
def error403(error):
    return render_template("error403.html"), 403

@app.errorhandler(404)
def error_404(error):
    return render_template("error404.html"), 404

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<page>")
def returnPage(page):
    #refuse strings that could alter path
    for entry in forbidden:
        if entry in page:
            abort(403)

    #change the string to support current pages path
    page = page + ".html"
    temp = "pages/" + page

    #check to see if that page exists
    if os.path.exists(temp):

        #return that page
        #default status code is already 200, but called explicitly
        return render_template(page), 200

    #if that page does not exist, return error 404
    else:
        abort(404)


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
